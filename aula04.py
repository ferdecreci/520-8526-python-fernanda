import os, math, sqlite3
import flask # Usado pra criar APIs
from random import randint

import modulo_baixado
modulo_baixado.imprime_mensagem()

from modulo_baixado import imprime_mensagem
imprime_mensagem()

# Funções -> Trechos de códigos que podem ser definidos pra serem usados e
# reutilizados no decorrer do programa.

def limpa_tela():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

def boas_vindas():
    nome = input("Qual é o seu nome? ")
    print(f"Seja muito bem vindo {nome}!")

def soma(*, x, y):
    resultado = x + y
    return resultado

print(soma(y=40, x=30))


# ======================================

def soma(x, y, /):
    resultado = x + y
    return resultado

print(soma(40, 30))

resp = int(input("Digite um numero: "))
resp2 = int(input("Digite outro numero: "))

variavel1 = soma(resp, resp2)

print(f"A soma de {resp} e {resp2} é {soma(resp, resp2)}")

# ==============================================
# Criando um UNICO parametro capaz de armazenar um numero indefinido de valores:
def mult(*x, y):
    for valor in x:
        print(f"{valor} x {y} = {valor * y}")

mult(10, 32, 2, 10, 12, y=5)

# ===============================================
# Criando um numero indefinido de parametros:
def diff(a, **chaves):
    for valor in chaves.values():
        print(f"{a} - {valor} = {a - valor}")

diff(a=75, b=20, c=45, d=120)
diff(a=10, b=20, c=45)
diff(a=10, b=20, c=45, d=120, e=500, f=450)

# ===============================================
#Funções Lambda / Funções anônimas
        
def soma1(x, y):
    return(x + y)

soma2 = lambda x, y: x + y

print(soma1(10, 15))
print(soma2(10, 15))

# ================================================
# Módulos

# Existem 3 maneiras de ter acesso a novos módulos no Python

# Existem módulos NATIVOS e carregados (print, input)
# Existem módulos NATIVOS mas não são importados por padrão (os, math, random)
# Existem módulos não NATIVOS que podem ser baixados pelo gerenciador de pacotes PIP (flask, pymongo)
# Baixando módulos criados e disponibilizados pela comunidade.

