# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

class Onibus:

    def __init__(self):
        self.capacidade_total = 45
        self.passageiros = 0
        self.movimento = False

    def embarcar(self, novos_passageiros):
        if self.movimento == True:
            return("O onibus está em movimento!")
        if self.passageiros == self.capacidade_total:
            return("O onibus já está lotado!")
        if self.passageiros + novos_passageiros > self.capacidade_total:
            total_passageiros =  self.passageiros + novos_passageiros
            passageiros_de_fora = total_passageiros - self.capacidade_total
            conseguiram_entrar = novos_passageiros - passageiros_de_fora

            self.passageiros = 45
            return(f"Apenas {conseguiram_entrar} passageiros conseguiram entrar no onibus, {passageiros_de_fora} ficaram de fora.")

        else:
            self.passageiros += novos_passageiros
            return(f"Todos os {novos_passageiros} passageiros conseguiram entrar no onibus.")

    def desembarcar(self, passageiros_saindo):
        if self.movimento == True:
            return("O onibus está em movimento!")
        if self.passageiros == 0:
            return("O onibus já está vazio!")
        if self.passageiros < passageiros_saindo:
            return(f"Há apenas {self.passageiros} pessoas no ônibus, não há como {passageiros_saindo} pessoas desembarcarem.")
        if self.passageiros > passageiros_saindo:
            self.passageiros -= passageiros_saindo
            return(f"{passageiros_saindo} passageiros sairam do onibus.")
        
    def acelerar(self):
        if self.movimento == False:
            self.movimento = True
            return("O onibus começou a se mover.")
        else:
            return("O onibus já está em movimento.")
        
    def frear(self):
        if self.movimento == True:
            self.movimento = False
            return("O onibus parou de se mover")
        else:
            return("O onibus já está parado.")


onibus_escolar = Onibus()

print(onibus_escolar.acelerar())
print(onibus_escolar.frear())
print(onibus_escolar.embarcar(15))
print(onibus_escolar.acelerar())
print(onibus_escolar.embarcar(5))
print(onibus_escolar.frear())
print(onibus_escolar.embarcar(45))
print(onibus_escolar.acelerar())
print(onibus_escolar.frear())
print(onibus_escolar.desembarcar(20))


# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class Fila:

    def __init__(self):
        self.fila = []         # Fila
        self.prioridade = []   # Apenas um registro pra controle

    def adicionar(self, idade):
        if idade >= 65:
            self.prioridade.append(idade)
            self.fila.append(idade)
        else:
            self.fila.append(idade)
        
        return(f"Uma pessoa de {idade} anos se juntou à fila.")
    
    def atender(self):
        if len(self.fila) == 0:
            return("Não há ninguém na fila.")

        atendido = self.fila[0]
        if atendido in self.prioridade:
            self.prioridade.remove(atendido)

        self.fila.remove(atendido)
        return(f"Uma pessoa com {atendido} anos foi atendida.")
    
    def dar_prioridade(self):
        if len(self.prioridade) == 0:
            return("Não há ninguem acima de 65 anos na fila.")
        prioritario = self.prioridade[0]
        self.prioridade.remove(prioritario)
        self.fila.remove(prioritario)
        self.fila.insert(0, prioritario)

        return(f"Uma pessoa com {prioritario} anos recebeu prioridade e passou na frente.")
    

banco = Fila()

print(banco.adicionar(17))
print(banco.adicionar(25))
print(banco.adicionar(82))
print(banco.adicionar(56))
print(banco.adicionar(71))
print(banco.adicionar(36))

print(banco.atender())
print(banco.dar_prioridade())
print(banco.atender())
