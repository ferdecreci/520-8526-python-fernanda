# Crie um script para simular uma lanchonete.
# 
# - Essa lanchonete deverá ter 5 tipos de comida e 3 tipos de bebidas no cardápio.
# - Quando clientes chegarem a essa lanchonete, eles deverão fazer um pedido aleatório de 1 comida + 1 bebida.
# - A lanchonete deverá ter um numero limitado de porções dessas comidas/bebidas. Comida = 5, bebida = 7.
# - Quando o cliente fazer o pedido, ele deverá ser notificado se as opções escolhidas estão disponiveis,
# e quais seus preços.

# Criar um DB.
# Criar uma tabela pra comida e uma pra bebida.
# Preencher essas tabelas.

# Criar uma função pra simular um cliente comprando algo.

import sqlite3
from random import choice

conexao = sqlite3.connect("estoque.db")
cursor = conexao.cursor()

tabela_comida = """
CREATE TABLE comidas (
id integer primary key autoincrement,
nome,
preco,
quantidade
)
"""

tabela_bebida = """
CREATE TABLE bebidas (
id integer primary key autoincrement,
nome,
preco,
quantidade
)
"""

comidas = [("Hamburguer", 25.00, 5), ("Hotdog", 12.00, 5), ("Misto quente", 7.50, 5), ("Chocolate", 5.00, 5), ("Pão", 1.50, 5)]
bebidas = [("Coca cola", 10.00, 7), ("Fanta", 8.00, 7), ("Café", 2.50, 5)]

try:
    cursor.execute(tabela_comida)
    cursor.execute(tabela_bebida)

    for comida in comidas:
        registra_estoque_comida = f"""
    INSERT INTO comidas (nome, preco, quantidade) 
    VALUES ("{comida[0]}", {comida[1]}, {comida[2]})"""    
        
        cursor.execute(registra_estoque_comida)

    for bebida in bebidas:
        registra_estoque_bebida = f"""
    INSERT INTO bebidas (nome, preco, quantidade) 
    VALUES ("{bebida[0]}", {bebida[1]}, {bebida[2]})"""    
        
        cursor.execute(registra_estoque_bebida)

    conexao.commit()

except sqlite3.OperationalError:
    pass

def checa_quantidade(tabela, pedido):
    if tabela == "comidas":
        checa_estoque = f'''SELECT quantidade FROM comidas WHERE nome = "{pedido}"'''
        cursor.execute(checa_estoque)
        conteudo = cursor.fetchall()

        if conteudo[0][0] == 0:
            return False
    
    elif tabela == "bebidas":
        checa_estoque = f'SELECT quantidade FROM bebidas WHERE nome = "{pedido}"'
        cursor.execute(checa_estoque)
        conteudo = cursor.fetchall()

        if conteudo[0][0] == 0:
            return False
    
    return True

def cliente():
    # 1 -> Selecionar aleatóriamente 1 comida e 1 bebida do estoque. (Pode-se fazer uso do módulo random)

    # 2 -> Checar se as comidas e bebidas estão disponiveis (Acessando o estoque com SELECT e checando o conteudo).

    # 3 -> Se a comida ou bebida não estiver mais em estoque, notificar o cliente.

    # 4 -> Se ambas estiverem disponiveis em estoque, fazer a venda, atualizando a quantidade dos items no estoque e
    #      notificando o cliente do preço a ser pago pelo pedido (Atualizando o estoque com UPDATE).

    pedido = {}
    pedido["comida"] = choice(comidas)[0]
    pedido["bebida"] = choice(bebidas)[0]

    if checa_quantidade("comidas", pedido["comida"]) == False:
        print(f'O item {pedido["comida"]} está fora de estoque.')

    elif checa_quantidade("bebidas", pedido["bebida"]) == False:
        print(f'O item {pedido["bebida"]} está fora de estoque.')
        
    else:
        print("Ambos os itens do pedido estão disponiveis.")

cliente()
cliente()
cliente()
cliente()
cliente()
