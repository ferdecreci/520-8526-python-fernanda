import os

# ======================================================================================================
# 3) Escreva um script em python que represente uma quitanda. O seu programa deverá
# apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
# deverá ser adicionada a uma cesta de compras.

# Menu principal:

# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair

# Menu de frutas:
# Digite a opção desejada:
# 1 - Banana
# 2 - Melancia
# 3 - Morango

# Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
# Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
# Digitado uma opção inválida

# O programa deverá ser encerrado apenas se o usuário digitar a opção 3.


cesta = []

while True:
    resposta = input("""
Quitanda:
1: Ver cesta
2: Adicionar frutas
3: Sair
""")

    if resposta == "1":

        if os.name == "posix":
            os.system("clear")
        else:
            os.system("cls")

        if len(cesta) == 0:
            print("Sua cesta ainda está vazia")
        else:
            print(f"A sua cesta atualmente possui: {cesta}")

    elif resposta == "2":

        if os.name == "posix":
            os.system("clear")
        else:
            os.system("cls")

        fruta = input(
"""Escolha uma fruta:
1 - Banana
2 - Melancia
3 - Morango
""")
        if fruta == "1":
            print("Adicionando uma banana à cesta")
            cesta.append("Banana")
        elif fruta == "2":
            print("Adicionando uma melancia à cesta")
            cesta.append("Melancia")
        elif fruta == "3":
            print("Adicionando um morango à cesta")
            cesta.append("Morango")
        else:
            print("Voce digitou um valor invalido")
    
    elif resposta == "3":

        if os.name == "posix": # Linux
            os.system("clear")
        else:                  # NT = Windows
            os.system("cls") 

        print("Parando o programa...")
        break

    else:
        print("Voce digitou um valor invalido")


 