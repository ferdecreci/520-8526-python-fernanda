# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.

# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:

# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'

def saudacao(nome):
    print(f"Olá {nome}! Tudo bem com voce?")

resposta = input("Qual é o seu nome? ")
saudacao(resposta)


# ====================================================
# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

def calculadora(x, y):
    operacao = int(input("""Escolha o que fazer com os numeros:
                         1 - Somá-los
                         2 - Subtraí-los
                         3 - Multiplicá-los
                         4 - Dividí-los\n\n"""))

    if operacao == 1:
        return(f"O resultado da soma entre {x} e {y} é {x+y}")
    elif operacao == 2:
        return(f"O resultado da diferença entre {x} e {y} é {x-y}")
    elif operacao == 3:
        return(f"O resultado da multiplicação entre {x} e {y} é {x*y}")
    elif operacao == 4:
        return(f"O resultado da divisão entre {x} e {y} é {x/y}")
    else:
        return("Opção inválida")

n1 = int(input("Escolha um numero: "))
n2 = int(input("Escolha outro: "))

print(calculadora(n1, n2))


# ====================================================
# Exercicio 3:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

def checa_par(x):
    contador = 0
    for number in x:
        if number % 2 == 0:
            contador += 1
    print(f"Foram encontrador {contador} numeros pares")

lista = []
while True:
    resp = input("Digite um numero para adiconar à lista e checar se é um par, ou digite PARAR para parar: ")
    if resp == "PARAR":
        break
    lista.append(int(resp))

checa_par(lista)

# ====================================================
# Exercicio 4:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações
# em funções, e adiconando um preço para cada fruta, de modo que quando a pessoa
# fizer o checkout ela consiga ver o preço total a pagar.

def limpa_tela():
    if os.name == "posix":
        os.system("clear")
    else:
        os.system("cls")

def adiciona_fruta(fruta):
    global checkout
    if fruta == "1":
        limpa_tela()
        print("Adicionando uma banana à cesta")
        cesta.append("Banana")
        checkout += 2.50

    elif fruta == "2":
        limpa_tela()
        print("Adicionando uma melancia à cesta")
        cesta.append("Melancia")
        checkout += 6.00

    elif fruta == "3":
        limpa_tela()
        print("Adicionando um morango à cesta")
        cesta.append("Morango")
        checkout += 1.50
    else:
        limpa_tela()
        print("Voce digitou um valor invalido")

cesta = []
checkout = 0.0

while True:
    resposta = input("""
Quitanda:
1: Ver cesta
2: Adicionar frutas
3: Sair
""")

    if resposta == "1":
        limpa_tela()
        if len(cesta) == 0:
            print("Sua cesta ainda está vazia")
        else:
            print(f"A sua cesta atualmente possui: {cesta}")

    elif resposta == "2":
        limpa_tela()
        fruta = input("""
Escolha uma fruta:
1 - Banana
2 - Melancia
3 - Morango
""")
        adiciona_fruta(fruta)
    
    elif resposta == "3":
        limpa_tela()
        print("Parando o programa...")
        print(f"O total a pagar é de R${checkout:.2f}")
        break

    else:
        limpa_tela()
        print("Voce digitou um valor invalido")
