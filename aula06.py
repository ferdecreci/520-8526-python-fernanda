# Programação orientada a objetos
# Classes (Tipos de objetos)

# Criação de uma classe (tipo de objeto) pra simular uma PILHA.

class Pilha:

    def __init__(self):
        self.pilha = list()
        self.limite = 5
        self.quantidade = 0

    def empilhar(self, item):
        if self.quantidade < self.limite:
            self.pilha.append(item)
            self.quantidade += 1
            return(f"{item} adicionado à pilha.")
        else:
            return(f"Essa pilha já tem 5 itens nela!")

    def desempilhar(self):
        if self.quantidade > 0:
            del self.pilha[-1]        #Deleção por palavra chave (del)
            # self.pilha.pop(-1)        #Deleção por método (pop)
            self.quantidade -= 1
            return(f"Item removido.")
        else:
            return(f"A pilha já está vazia!")


pilha_de_pratos = Pilha()
pilha_de_brinquedos = Pilha()
pilha_de_roupas = Pilha()

print(pilha_de_roupas.limite)

print(pilha_de_pratos.empilhar("prato de porcelana"))
print(pilha_de_pratos.empilhar("prato de vidro"))
print(pilha_de_pratos.empilhar("prato de madeira"))
print(pilha_de_pratos.empilhar("prato de metal"))
print(pilha_de_pratos.empilhar("prato de plastico"))
print(pilha_de_pratos.empilhar("prato de barro"))
print(pilha_de_pratos.desempilhar())
print(pilha_de_pratos.desempilhar())
print(pilha_de_pratos.desempilhar())

# =======================================================================
# Encapsulamento

class Pilha:

    def __init__(self):
        self.__pilha = list()
        self.__limite = 5
        self.__quantidade = 0

    def empilhar(self, item):
        if self.__quantidade < self.__limite:
            self.__pilha.append(item)
            self.__quantidade += 1
            return(f"{item} adicionado à pilha.")
        else:
            return(f"Essa pilha já tem 5 itens nela!")

    def desempilhar(self):
        if self.__quantidade > 0:
            del self.__pilha[-1]        #Deleção por palavra chave (del)
            # self.__pilha.pop(-1)        #Deleção por método (pop)
            self.__quantidade -= 1
            return(f"Item removido.")
        else:
            return(f"A pilha já está vazia!")


pilha_de_roupas = Pilha()
# print(pilha_de_roupas.limite)

# =======================================================================
# Herança

class Funcionario:

    def __init__(self):
        self.salario = 0

    def define_salario(self, salario):
        self.salario = salario


class Gerente(Funcionario):

    def __init__(self):
        super().__init__()
        self.bonus = 1.3

    def aplica_bonus(self):
        self.salario = self.salario * self.bonus


joao = Funcionario()
joao.define_salario(2000)
print(joao.salario)
# joao.aplica_bonus()

pedro = Gerente()
pedro.define_salario(3000)
pedro.aplica_bonus()
print(pedro.salario)

# =======================================================================
# Polimorfismo

class Funcionario:

    def __init__(self):
        self.salario = 2500
        self.nome = ""
        self.idade = ""

    def define_salario(self, salario):
        self.salario = salario


class Gerente(Funcionario):

    def __init__(self):
        super().__init__()
        self.salario = self.salario * 1.3

# ======================================================================
