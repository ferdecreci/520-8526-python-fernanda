import csv

# Trabalhar com arquivos
# w - write  - escrever
# r - read   - ler
# a - append - adicionar 

arquivo = open("teste.txt", "a")
conteudo = "Primeira linha do meu arquivo\n"
arquivo.write(conteudo)
arquivo.close()

arquivo = open("teste.txt", "r")
conteudo = arquivo.read()
print(conteudo)
arquivo.close()

with open("teste.txt", "a") as arquivo:
    conteudo = "Linha adicionada com o WITH\n"
    arquivo.write(conteudo)

# ================================================
#csv
    
with open("arquivo.csv", "r") as planilha:
    conteudo = csv.reader(planilha, delimiter=";")
    # Generator
    print(next(conteudo))
    for linha in conteudo:
        print(linha)

# ================================================
