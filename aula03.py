# Colecoes
# Tuplas , Listas , Dic , Sets

# ========
# Tuplas ( uma colecao de objetos)

variavel1 = ("Arroz", "Queijo", "leite", "beterraba")
# Index   Posicao 0      1         2          3


print(variavel1)

for item in variavel1:
    print(item)

# Tuplas são immutaveis , e tem index
    
    print(variavel1[3])

# Listas . Difereca de listas e tuplas , listas podem ser alteradas , tuplas nao
    variavel2 = ["arroz","Queijo", "leite", "beterraba" , 50, False, "arroz"]

    print(variavel2[3])

    for item in variavel2:
        print(item)

    # para saber o numero do objeto do index
        
     #   print(variavel2.index("arroz")) 
     #   print(variavel2.count("arroz"))
     #   variavel2.remove("beterraba")   # remove pelo nome
     #   variavel2.pop(2)    # remove pelo numero do index

     #   variavel2.append("Cachorro quente")  # adiciona um item no final
     #   variavel2.insert(0, "Faca")          # adiciona um item no index  escolhido

     #   print(variavel2)
        
        # Dicionarios ou  Hash Tables , simbolo {}

#variavel3 = { "Chave": "valor"}
        variavel3 = { "idade": 13, "cor": "rosa", "animal" :"cachorro"}
        # 3 item em pares (idade/13 , cor/rosa , animal/cachorro) chave/valor

        variavel3["nome"] = "Fernanda"  #adicionando 1 item no dic
        variavel3.pop("idade")     #removendo um valor
        del(variavel3["cor"])      #removendo um valor funcao del
        print(variavel3)

        # for objeto in  variavel3.values():
        # print(objeto)

        # Nao pode haver chaves repetidas em um dicionario

        #Sets: n permite valor repetido

        variavel4 = { "idade", " cachorro" , 12, False , 550, "idade"}
        print(variavel4)

        variavel4 = set(variavel4)
        variavel4 = list(variavel4)
        print(variavel4)
        





