# 1) Escreva um script em Python que substitua o caractere “x” por espaço considerando a
# seguinte frase:
# “Umxpratoxdextrigoxparaxtrêsxtigresxtristes”


# ======================================================================================================
# 2) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z


#Exec 1
nome = "Umxpratoxdextrigoxparaxtrêsxtigresxtristes"

print(nome.replace("x", " "))

# Exec 2

nasc = int(input("em que ano vc nasceu?"))

if nasc <= 1964:
    print ("Baby Boomer")

elif nasc <= 1979:
    print ("Geracao X")
elif nasc <= 1994:
    print ("Geracao Y")
else:
    print("geracao Z")
