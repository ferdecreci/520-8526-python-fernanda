# Tipos Primitivos - Tipos de dados basicos do python

# String  -> str   -> Frases ou conjunto de caracteres ,sempre entre aspas
# Integer -> int   -> numeros inteiros, sem aspas
# Float   -> float -> numeros decimais, numeros reais , numeros com virgula
# Boolean -> bool  -> True or False tem q comecarco mletra maiuscula

#Variaveis de exemplo
nome = " Fernanda L. Decreci" #STRING
idade = 35                    #INTEGER
altura = 1.67                 #FLOAT
acordado = True               #BOOL


numero1 = "10"
numero2 = "25"
numero3 = numero1 + numero2
print(numero3)

numero1 = 10
numero2 = 25
numero3 = numero1 + numero2
print(numero3)


frase = "Uma gota d'agua"
frase2 = 'ele disse "acorda" '
frase3 = """" oi "ou" asssim """


# Operadores aritimericos
# + - * /

# Operadores de comparacao

#  igualdade -> ==
#  desigualdade -> !=


print(numero1 == numero2)
print(numero1 != numero2)


# maior que ->    >
# menor que ->    <
# menor ou igual ->   <=
# maoir ou igual ->   >=



print(numero1 < numero2)

#Variaveis de exemplo
nome = " Fernanda L. Decreci" #STRING
idade = 35                    #INTEGER
altura = 1.67                 #FLOAT
acordado = True               #BOOL

idade = str(idade)
idade = int(idade)
idade = float(idade)

# Tipos Primitivos - Tipos de dados básicos do python

# String  -> str   -> Frases, ou cunjunto de caracteres, sempre entre aspas.
# Integer -> int   -> Numeros inteiros, sem aspas.
# Float   -> float -> Numeros decimais, numeros reais, numeros com virgula.
# Boolean -> bool  -> True ou False

nome = "Tiago P. Lima" # STRING
idade = 27             # INTEGER
altura = 1.79          # FLOAT
acordado = True        # BOOL

# =========================================

numero1 = 10
numero2 = 25

numero3 = numero1 + numero2
# print(numero3)

frase = "Uma gota d'agua"
frase2 = 'Ele disse que estava "Acordado" hoje de manhã'

frase3 = """Hoje o dia está muito frio
Porém amanhã vai estar mais quente
ontem estava gelado"""

# ==========================================

# Operadores aritiméticos

# + - * /

# Operadores de Comparação

# igualdade    ->  ==
# desigualdade ->  !=
# print(numero1 != numero2)

# maior que ->   >
# menor que ->   <
# print(numero1 < numero2)

# menor ou igual ->  <=
# maior ou igual ->  >=
# print(numero1 <= numero2)

# =========================================

nome = "Tiago P. Lima" # STRING
idade = 27             # INTEGER
altura = 1.79          # FLOAT
acordado = True        # BOOL

idade = str(idade)
idade = int(idade)
idade = float(idade)


# ========================================

# Métodos de strings

nome = "Tiago P. Lima"

# print(nome.replace("T", "G"))

# ========================================

# Estruturas de Decisão

# idade = int(input("Qual a sua idade? "))

# if idade >= 18:
#     print("Voce pode entrar e comprar bebidas no bar.")

# elif idade >=15:
#     print("Voce pode entrar, mas não pode comrpar bebidas.")

# else:
#     print("Voce não pode entrar!")

# Estruturas de Repetição
# WHILE - FOR
    
# contador = 0
# while contador < 10:
#     print("contando 10 vezes")
#     contador = contador + 1

while True:
    resposta = input("Voce quer parar o programa? [S/N] ")
    resposta = resposta.upper()

    if resposta == "S":
        break


for letra in range(0, 15):
    print(letra)



